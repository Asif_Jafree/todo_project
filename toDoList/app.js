let inputEle = document.querySelector('.inputbox');
let todoEle = document.querySelector('.todoUl');
let addButton = document.getElementById('add');

//console.log(addButton);
//addButton.setAttribute('onclick', 'addListByButton()');
let arr = [];
if (localStorage.length == 0) {
  localStorage.setItem('todoItems', '[]');
}
arr = [...JSON.parse(localStorage.getItem('todoItems'))];

function createNewList(text, id, checked) {
  let newList = document.createElement('li');
  newList.setAttribute('class', 'toDo-list');

  let input = document.createElement('input');
  input.setAttribute('type', 'checkbox');
  input.setAttribute('class', 'checkbox');
  if (checked) {
    newList.setAttribute('class', 'strike');
    input.checked = true;
  } else {
    newList.classList.remove('strike');
    input.checked = false;
  }

  let deleteBtn = document.createElement('button');
  deleteBtn.setAttribute('class', 'delete');
  deleteBtn.setAttribute('id', id);
  deleteBtn.appendChild(document.createTextNode('Delete'));

  let editButton = document.createElement('button');
  editButton.setAttribute('class', 'edit');
  editButton.setAttribute('id', id);
  editButton.appendChild(document.createTextNode('Edit'));

  newList.appendChild(input);
  newList.appendChild(document.createTextNode(text));
  newList.appendChild(deleteBtn);
  newList.appendChild(editButton);
  deleteBtn.addEventListener('click', deleteList);
  editButton.addEventListener('click', editList);
  if (id != undefined) {
    newList.setAttribute('id', id);
    todoEle.appendChild(newList);
  }
  return newList;
}

function addList() {
  inputEle.addEventListener('keypress', function (event) {
    if (event.keyCode === 13 && inputEle.value) {
      let randomId =
        Math.floor(Math.random() * 10000) + inputEle.value.slice(0, 3);
      let element = createNewList(inputEle.value, randomId, false);
      let id = 'todoItems';
      let task = inputEle.value;
      let status = false;
      let obj = { id: randomId, todo: task, checked: status };
      arr.push(obj);
      localStorage.setItem(id, JSON.stringify(arr));
      todoEle.appendChild(element);
      inputEle.value = '';
    }
  });
}
function addbut() {
  addButton.addEventListener('click', function (event) {
    if (inputEle.value) {
      let randomId =
        Math.floor(Math.random() * 10000) + inputEle.value.slice(0, 3);
      let element = createNewList(inputEle.value, randomId);
      let id = 'todoItems';
      let task = inputEle.value;
      let status = false;
      let obj = { id: randomId, todo: task, checked: status };
      arr.push(obj);
      localStorage.setItem(id, JSON.stringify(arr));
      todoEle.appendChild(element);
      inputEle.value = '';
    }
  });
}
addbut();
function deleteList(event) {
  let targetElement = event.target;
  let list = targetElement.parentNode;
  list.remove();
  let localStorageLength = JSON.parse(localStorage.getItem('todoItems')).length;
  for (let index = 0; index < localStorageLength; index++) {
    if (JSON.parse(localStorage.getItem('todoItems'))[index]['id'] == list.id) {
      let deleteElement = arr.splice(index, 1);
      localStorage.setItem('todoItems', JSON.stringify(arr));
      break;
    }
  }
}
function editList(event) {
  let targetElement = event.target;
  let list = targetElement.parentNode;
  list.remove();
  let localStorageLength = JSON.parse(localStorage.getItem('todoItems')).length;
  for (let index = 0; index < localStorageLength; index++) {
    if (JSON.parse(localStorage.getItem('todoItems'))[index]['id'] == list.id) {
      inputEle.value = JSON.parse(localStorage.getItem('todoItems'))[index][
        'todo'
      ];
      let deleteElement = arr.splice(index, 1);
      localStorage.setItem('todoItems', JSON.stringify(arr));
      break;
    }
  }
}

function checkbox() {
  todoEle.addEventListener('change', function (e) {
    // console.log(e.target.parentNode.id);

    let target = e.target;

    if (
      target.classList.contains('checkbox') &&
      target.parentNode.classList != 'strike'
    ) {
      target.parentNode.classList = 'strike';

      let localStorageLength = JSON.parse(localStorage.getItem('todoItems'))
        .length;
      for (let index = 0; index < localStorageLength; index++) {
        if (
          JSON.parse(localStorage.getItem('todoItems'))[index]['id'] ==
          e.target.parentNode.id
        ) {
          arr[index]['checked'] = true;
          localStorage.setItem('todoItems', JSON.stringify(arr));
          break;
        }
      }
    } else {
      target.parentNode.classList = 'removeStrike';

      let localStorageLength = JSON.parse(localStorage.getItem('todoItems'))
        .length;
      for (let index = 0; index < localStorageLength; index++) {
        if (
          JSON.parse(localStorage.getItem('todoItems'))[index]['id'] ==
          e.target.parentNode.id
        ) {
          arr[index]['checked'] = false;
          localStorage.setItem('todoItems', JSON.stringify(arr));
          break;
        }
      }
    }
  });
}

function pageReload() {
  let value = JSON.parse(localStorage.getItem('todoItems'));
  for (let index = 0; index < value.length; index++) {
    let key = value[index]['id'];
    let task = value[index]['todo'];
    let checked = value[index]['checked'];
    createNewList(task, key, checked);
  }
  inputEle.value = '';
}
pageReload();
addList();
addList();
checkbox();
